FROM node:alpine as npmbuild
ARG outdir=assets/js/
WORKDIR /app
COPY package.json ./
RUN npm install
RUN mkdir -p ${outdir}

FROM ruby:2.6
WORKDIR /app
EXPOSE 4000
COPY Gemfile ./
RUN gem install bundler
RUN bundle install
COPY . .
COPY --from=npmbuild /app/node_modules ./node_modules
RUN cp node_modules/bootstrap/dist/js/bootstrap.min.js assets/js/bootstrap.min.js
RUN cp node_modules/jquery/dist/jquery.min.js assets/js/jquery.min.js
RUN cp node_modules/popper.js/dist/popper.min.js assets/js/popper.min.js
RUN bundle exec jekyll build
CMD bundle exec jekyll serve -D