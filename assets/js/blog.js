$(document).ready(function () {
	$(".previous").each(function () {
		$(this).inViewport(function (px) {
			if (px) $(this).addClass('animated fadeInLeftBig');
		});
	});

	$(".next").each(function () {
		$(this).inViewport(function (px) {
			if (px) $(this).addClass('animated fadeInRightBig');
		});
	});
});