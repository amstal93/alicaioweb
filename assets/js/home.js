$(document).ready(function () {
	$(".has-animation").each(function (index) {
		$(this)
			.delay($(this).data("delay"))
			.queue(function () {
				$(this).addClass("animate-in");
			});
	});

	$(".post-preview").each(function () {
		$(this).inViewport(function (px) {
			if (px) $(this).addClass('animated fadeInLeftBig');
		});
	});

});